package com.arvisapps.aryansh.encryptit;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends Activity
    {
        int l1 = 0;
        int l2 = 0;
        String usernameValue;
        String passwordValue;
        String valueStr;
        String valueStr2;
        SharedPreferences.Editor editor;
        SharedPreferences prefs;
        Button loginBtn;

      @Override
        protected void onCreate(Bundle savedInstanceState)
      {
          prefs = getSharedPreferences("EncryptIt", MODE_PRIVATE);
          valueStr = prefs.getString("u_key", usernameValue);
          Log.d("ARVIS APPS", "username caught - " + valueStr);
          valueStr2 = prefs.getString("p_key", passwordValue );
          Log.d("ARVIS APPS", "password caught - " + valueStr2);
          if(valueStr != null)
          {
              l1 = valueStr.length();
              Log.d("ARVIS APPS", "username length - " + l1);
          }
          if(valueStr2 != null)
          {
              l2 = valueStr2.length();
              Log.d("ARVIS APPS", "password length - " + l2);
          }

          if(l1 != 0 && l2 != 0)
          {


                  Intent intent = new Intent(MainActivity.this, TweetListActivity.class);
                  startActivity(intent);
                  finish();

          }

          /*else if(valueStr.equals(null) && valueStr2.equals(null))
          {*/
              super.onCreate(savedInstanceState);
              setContentView(R.layout.activity_main);
              loginBtn = (Button) findViewById(R.id.btn_login);


              loginBtn.setOnClickListener(new View.OnClickListener()
              {
                  @Override
                  public void onClick(View v)
                  {
                  /*Intent intent = new Intent(MainActivity.this, TweetListActivity.class);
                  startActivity(intent);*/

                      EditText username = (EditText) findViewById(R.id.fld_username);
                      usernameValue = username.getText().toString();
                      EditText password = (EditText) findViewById(R.id.fld_pwd);
                      passwordValue = password.getText().toString();

                  /*Log.d("Aryansh", "username caught - " + usernameValue);*/

                      prefs = getSharedPreferences("EncryptIt", MODE_PRIVATE);

                      editor = prefs.edit();
                      editor.putString("u_key", usernameValue);
                      editor.apply();
                      editor.putString("p_key", passwordValue);
                      editor.apply();

                      valueStr = prefs.getString("u_key", null);
                      valueStr2 = prefs.getString("p_key", null);

                      Intent intent = new Intent(MainActivity.this, TweetListActivity.class);
                      startActivity(intent);
                  }

              });
         /* }*/
      }
}
