package com.arvisapps.aryansh.encryptit;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class TweetListActivity extends ListActivity
{
        private String[] stringArray;
        private ArrayAdapter tweetItemArrayAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweet_list);

        stringArray = new String[10];
        for(int i=0; i<stringArray.length; i++)
        {
            stringArray[i]="String " + i;
        }

        tweetItemArrayAdapter = new TweetAdapter(this, stringArray);
            setListAdapter(tweetItemArrayAdapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id)
    {
        Intent intent = new Intent(this, TweetDetailActivity.class);
        startActivity(intent);
    }

}